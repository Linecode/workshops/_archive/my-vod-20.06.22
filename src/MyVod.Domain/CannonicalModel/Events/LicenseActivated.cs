using MediatR;
using MyVod.Domain.Legal.Domain;

namespace MyVod.Domain.CannonicalModel.Events;

public record LicenseActivated(Guid LicenseId, Guid MovieId, DateRange DateRange, RegionIdentifier RegionIdentifier) : INotification;

public record LicenseDeActivated(Guid Movie) : INotification;