using MediatR;
using MyVod.Domain.Legal.Domain;

namespace MyVod.Domain.CannonicalModel.Events;

public record LicenseCreated(Guid LicenseId, Guid MovieId, DateRange DateRange, RegionIdentifier RegionIdentifier) : INotification
{}