﻿using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain;

public class Email : ValueObject<Email>
{
    public string Value { get; private set; }

    [Obsolete("Only for EF Core", true)]
    private Email()
    { }
    
    private Email(string value)
    {
        Value = value;
    }

    public static Email Create(string value)
    {
        Ensure.That(value).IsNotNullOrWhiteSpace();
        Ensure.That(value.Contains("@")).IsTrue();
        
        return new Email(value);
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }

    public static Email TestEmail => new Email("test@test.pl");
}

