using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.Legal.Infrastructure;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Legal.Application;

public interface ILicesnseApplicationService : IApplicationService
{
    Task AddLicense(DateRange dateRange, Guid movieId, RegionIdentifier regionIdentifier);
    Task<License> GetLicense(Guid id);
}

public class LicesnseApplicationService : ILicesnseApplicationService
{
    private readonly ILicenseRepository _repository;

    public LicesnseApplicationService(ILicenseRepository repository)
    {
        _repository = repository;
    }

    public async Task AddLicense(DateRange dateRange, Guid movieId, RegionIdentifier regionIdentifier)
    {
        var license = new License(dateRange, regionIdentifier, movieId);

        await _repository.Add(license);
    }

    public async Task<License> GetLicense(Guid id)
    {
        var result = await _repository.Get(id);

        return result;
    }
}