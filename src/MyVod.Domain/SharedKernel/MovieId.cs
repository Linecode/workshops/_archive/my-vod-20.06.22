using System.ComponentModel;
using System.Globalization;
using MyVod.Domain.MovieCatalog.Domain;
using Newtonsoft.Json;

namespace MyVod.Domain.SharedKernel;

[TypeConverter(typeof(MovieIdTypeConverter))]
[JsonConverter(typeof(MovieIdJsonConverter))]
public readonly struct MovieId : IComparable<MovieId>, IEquatable<MovieId>
{
    public Guid Value { get; }

    public static MovieId New() => new MovieId(Guid.NewGuid());
    public static MovieId Parse(string value) => new(Guid.Parse(value));

    public MovieId(Guid value) => Value = value;

    public bool Equals(MovieId other) => Value.Equals(other.Value);
    public override bool Equals(object obj) => obj is MovieId other && Equals(other);
    public int CompareTo(MovieId other) => Value.CompareTo(other.Value);
    public override int GetHashCode() => Value.GetHashCode();
    public override string ToString() => $"MovieId: {Value.ToString()}";
    public static bool operator ==(MovieId a, MovieId b) => a.CompareTo(b) == 0;
    public static bool operator !=(MovieId a, MovieId b) => !(a == b);

    private class MovieIdTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var stringValue = value as string;
            if (!string.IsNullOrEmpty(stringValue)
                && Guid.TryParse(stringValue, out var guid))
            {
                return new MovieId(guid);
            }

            return base.ConvertFrom(context, culture, value);
        }
    }

    private class MovieIdJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(MovieId);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var id = (MovieId)value;
            serializer.Serialize(writer, id.Value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var guid = serializer.Deserialize<Guid>(reader);
            return new MovieId(guid);
        }
    }
}