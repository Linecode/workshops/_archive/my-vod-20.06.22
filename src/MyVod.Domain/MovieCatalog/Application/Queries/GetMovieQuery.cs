using MediatR;
using MyVod.Domain.MovieCatalog.Domain;
using MyVod.Domain.MovieCatalog.Infrastructure;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MovieCatalog.Application.Queries;

public record GetMovieQuery(MovieId Id) : IRequest<Movie>;

internal class GetMovieHandler : IRequestHandler<GetMovieQuery, Movie>
{
    private readonly IMoviesRepository _repository;

    public GetMovieHandler(IMoviesRepository repository)
    {
        _repository = repository;
    }
    
    public async Task<Movie> Handle(GetMovieQuery request, CancellationToken cancellationToken)
    {
        var result = await _repository.Get(request.Id);

        if (result is null)
            throw new Exception("Oh no :(");
        
        return result;
    }
}