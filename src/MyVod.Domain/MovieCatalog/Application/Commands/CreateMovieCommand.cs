using MediatR;
using MyVod.Domain.MovieCatalog.Domain;
using MyVod.Domain.MovieCatalog.Infrastructure;

namespace MyVod.Domain.MovieCatalog.Application.Commands;

public record CreateMovieCommand(string Title) : IRequest;

internal class CreateMovieHandler : IRequestHandler<CreateMovieCommand>
{
    private readonly IMoviesRepository _repository;

    public CreateMovieHandler(IMoviesRepository repository)
    {
        _repository = repository;
    }
    
    public async Task<Unit> Handle(CreateMovieCommand request, CancellationToken cancellationToken)
    {
        var title = new Title("", "");
        var movie = new Movie(title, new Description(""));

        await _repository.Add(movie);
        
        return Unit.Value;
    }
}
 