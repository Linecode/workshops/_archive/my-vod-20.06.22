using MediatR;
using MyVod.Domain.MovieCatalog.Infrastructure;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MovieCatalog.Application.Commands;

public record PublishMovieCommand(MovieId Id) : IRequest;

internal class PublishMovieHandler : IRequestHandler<PublishMovieCommand>
{
    private readonly IMoviesRepository _repository;

    public PublishMovieHandler(IMoviesRepository repository)
    {
        _repository = repository;
    }
    
    public async Task<Unit> Handle(PublishMovieCommand request, CancellationToken cancellationToken)
    {
        var movie = await _repository.Get(request.Id);
        
        movie!.Publish();

        _repository.Update(movie);
        await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        
        return Unit.Value;
    }
}