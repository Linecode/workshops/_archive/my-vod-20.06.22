using MyVod.Domain.MovieCatalog.Infrastructure;

namespace MyVod.Domain.MovieCatalog.Application;

public class MoviesService
{
    private readonly IMoviesRepository _moviesRepository;

    public MoviesService(IMoviesRepository moviesRepository)
    {
        _moviesRepository = moviesRepository;
    }
}