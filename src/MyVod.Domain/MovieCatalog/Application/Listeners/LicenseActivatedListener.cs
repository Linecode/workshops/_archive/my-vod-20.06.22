using MediatR;
using MyVod.Domain.CannonicalModel.Events;
using MyVod.Domain.MovieCatalog.Application.Commands;
using MyVod.Domain.MovieCatalog.Infrastructure;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MovieCatalog.Application.Listeners;

public class LicenseActivatedListener : INotificationHandler<LicenseActivated>
{
    private readonly IMoviesRepository _repository;
    private readonly ISender _sender;

    public LicenseActivatedListener(IMoviesRepository repository, ISender sender)
    {
        _repository = repository;
        _sender = sender;
    }
    
    public async Task Handle(LicenseActivated notification, CancellationToken cancellationToken)
    {
        await _sender.Send(new PublishMovieCommand(new MovieId(notification.MovieId)), cancellationToken);
    }
}