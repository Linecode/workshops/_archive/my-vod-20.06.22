using System.Linq.Expressions;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.MovieCatalog.Domain.Specifications;

public class PublishedMovieSpecification : Specification<Movie>
{
    public override Expression<Func<Movie, bool>> ToExpression()
        => movie => movie.Status == Movie.MovieStatus.Published;
}

public class TitleStartFromMovieSpecification : Specification<Movie>
{
    private readonly string _startFrom;

    public TitleStartFromMovieSpecification(string startFrom)
    {
        _startFrom = startFrom;
    }
    
    public override Expression<Func<Movie, bool>> ToExpression()
        => movie => movie.Title.Original.StartsWith(_startFrom);
}
