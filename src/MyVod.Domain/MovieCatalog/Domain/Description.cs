using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.MovieCatalog.Domain;

public class Description : ValueObject<Description>
{
    public string Value { get; private set; }

    [Obsolete("Only For EF", true)]
    private Description()
    { }
    
    public Description(string description)
    {
        Ensure.That(description).HasLengthBetween(10, 5000);
        
        Value = description;
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
}