using System.ComponentModel;
using System.Globalization;
using Newtonsoft.Json;

namespace MyVod.Domain.MovieCatalog.Domain;

[TypeConverter(typeof(DirectorIdTypeConverter))]
[JsonConverter(typeof(DirectorIdJsonConverter))]
public readonly struct DirectorId : IComparable<DirectorId>, IEquatable<DirectorId>
{
    public Guid Value { get; }
    
    public static DirectorId New() => new(Guid.NewGuid());
    public static DirectorId Parse(string id) => new(Guid.Parse(id)); 

    public DirectorId(Guid value) => Value = value;

    public bool Equals(DirectorId other) => Value.Equals(other.Value);
    public override bool Equals(object obj) => obj is DirectorId other && Equals(other);
    public int CompareTo(DirectorId other) => Value.CompareTo(other.Value);
    public override int GetHashCode() => Value.GetHashCode();
    public override string ToString() => $"DirectorId: {Value.ToString()}";
    public static bool operator ==(DirectorId a, DirectorId b) => a.CompareTo(b) == 0;
    public static bool operator !=(DirectorId a, DirectorId b) => !(a == b);

    private class DirectorIdTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var stringValue = value as string;
            if (!string.IsNullOrEmpty(stringValue)
                && Guid.TryParse(stringValue, out var guid))
            {
                return new DirectorId(guid);
            }

            return base.ConvertFrom(context, culture, value);
        }
    }
    
    private class DirectorIdJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
            => objectType == typeof(DirectorId);

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var id = (DirectorId)value;
            serializer.Serialize(writer, id.Value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var guid = serializer.Deserialize<Guid>(reader);
            return new DirectorId(guid);
        }
    }
}