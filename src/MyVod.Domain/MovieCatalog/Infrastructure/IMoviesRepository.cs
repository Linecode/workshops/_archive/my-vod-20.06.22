using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain.MovieCatalog.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MovieCatalog.Infrastructure;

public interface IMoviesRepository : IRepository
{
    IUnitOfWork UnitOfWork { get; }
    Task Add(Movie movie);
    Task<Movie?> Get(MovieId id);
    Task<IEnumerable<Movie?>> Get(Specification<Movie> spec);
    void Update(Movie movie);
}
