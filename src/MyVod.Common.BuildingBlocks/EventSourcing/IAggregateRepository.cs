namespace MyVod.Common.BuildingBlocks.EFCore;

public interface IAggregateRepository
{
    Task Store(AggregateBase aggregate, CancellationToken cancellationToken = default);
    Task<T> Load<T>(Guid id, int? version = null, CancellationToken cancellationToken = default) where T : AggregateBase;
}
