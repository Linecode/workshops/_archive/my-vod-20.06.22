namespace MyVod.Common.BuildingBlocks.Ddd;

public interface IEntity
{
    IReadOnlyList<object> DomainEvents { get; }
    void RemoveDomainEvent(object eventItem);
    void ClearDomainEvents();
}