using MyVod.Domain.CannonicalModel.PublishedLanguage;
using MyVod.Domain.Sales.Application.Ports;

namespace MyVod.StripePaymentGateway;

public class StripePaymentService : IPaymentGateway
{
    public async Task<PaymentIntend> PayOrder(string name, long price, string currency, Guid orderId)
    {
        await Task.Delay(1000);

        return new PaymentIntend(Guid.NewGuid().ToString(), new Uri("https://test.test.pl"));
    }
}