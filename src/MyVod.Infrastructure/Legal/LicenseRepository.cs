using MediatR;
using MongoDB.Driver;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.Legal.Infrastructure;

namespace MyVod.Infrastructure.Legal;

public class LicenseRepository : ILicenseRepository
{
    private readonly IPublisher _publisher;
    private readonly IMongoDatabase Database;
    private const string CollectionName = "licenses";

    public LicenseRepository(IMongoClient mongoClient, IPublisher publisher)
    {
        _publisher = publisher;
        Database = mongoClient.GetDatabase("licenses");
    }

    public async Task Add(License license)
    {
        var collection = Database.GetCollection<License>(CollectionName);

        await collection.InsertOneAsync(license);

        var domainEvents = license.DomainEvents;
        license.ClearDomainEvents();

        var tasks = domainEvents.Select(async domainEvent =>
        {
            await _publisher.Publish(domainEvent).ConfigureAwait(false);
        });

        await Task.WhenAll(tasks).ConfigureAwait(false);
    }

    public async Task<License> Get(Guid id)
    {
        var collection = Database.GetCollection<License>(CollectionName);

        var result = await collection.Find(x => x.Id.Equals(id)).Limit(1).ToListAsync();

        return result.SingleOrDefault()!;
    }
}