using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.Legal;

public static class LegalMappings
{
    public static void RegisterMappings()
    {
        BsonClassMap.RegisterClassMap<Entity<Guid>>(e =>
        {
            e.AutoMap();
            
            e.SetIdMember(e.GetMemberMap(x => x.Id).SetSerializer(new GuidSerializer(BsonType.String)));
            
            e.UnmapMember(x => x.DomainEvents);
        });

        BsonClassMap.RegisterClassMap<License>(l =>
        {
            l.MapProperty(x => x.Status)
                .SetSerializer(new EnumSerializer<License.LicenseStatus>());
            l.MapProperty(x => x.DateRange);
            l.MapProperty(x => x.MovieId)
                .SetSerializer(new GuidSerializer(BsonType.String));
            l.MapProperty(x => x.RegionIdentifier);
        });
    }
}