using MediatR;
using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.Extensions;
using MyVod.Domain.MovieCatalog.Domain;

namespace MyVod.Infrastructure.MoviesCatalog;

public class MovieDbContext : DbContext, Common.BuildingBlocks.EFCore.IUnitOfWork
{
    private readonly IMediator _mediator;
    public DbSet<Movie?> Movies { get; set; }

    public MovieDbContext(DbContextOptions<MovieDbContext> options, IMediator mediator) : base(options)
    {
        _mediator = mediator;
    }
    
    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await _mediator.DispatchDomainEventsAsync<MovieDbContext>(this);

        await SaveChangesAsync(cancellationToken);

        return true;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Config
        modelBuilder.ApplyConfiguration(new DirectorEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new MovieEntityTypeConfiguration());
        
        base.OnModelCreating(modelBuilder);
    }
}