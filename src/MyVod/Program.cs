using System.Reflection;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Data;
using MyVod.Domain;
using MyVod.Infrastructure;
using MyVod.Infrastructure.Legal;
using MyVod.Infrastructure.MoviesCatalog;
using MyVod.Services;
using MyVod.StripePaymentGateway;
using OrderService = MyVod.Services.OrderService;
using PersonService = MyVod.Services.PersonService;
using StripeConfiguration = Stripe.StripeConfiguration;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(connectionString));
builder.Services.AddDbContext<MoviesDbContext>(options =>
{
    options.UseSqlite(connectionString, 
        x => x.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
});
builder.Services.AddDbContext<MovieDbContext>(options =>
{
    options.UseSqlite(connectionString, 
        x => x.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
});
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<ApplicationDbContext>();
builder.Services.AddControllersWithViews()
    .AddRazorRuntimeCompilation()
    .AddNewtonsoftJson();


StripeConfiguration.ApiKey =
    "sk_test_51L3x0jIP4aoyQ2vi0RgDKWCPmI22ugYLYJbCeUBrgfYMEE0HBegyc5sJDt06xeEc1PQVtXqv56by02gLDwXZyU8Q00QT28pWGf";

builder.Services.AddScoped<IMovieService, MovieService>();
builder.Services.AddScoped<IGenreService, GenreService>();
builder.Services.AddScoped<IPersonService, PersonService>();
builder.Services.AddScoped<IOrderService, OrderService>();

builder.Services.Scan(scan => scan.FromAssembliesOf(typeof(DomainAssemblyInfo), typeof(InfrastructureAssemblyInfo))
    .AddClasses(classes => classes.AssignableTo<IApplicationService>())
    .AsImplementedInterfaces().WithScopedLifetime()
    .AddClasses(classes => classes.AssignableTo<IRepository>())
    .AsImplementedInterfaces().WithScopedLifetime());
builder.Services.AddMediatR(typeof(DomainAssemblyInfo));

LegalMappings.RegisterMappings();
builder.Services.AddSingleton<IMongoClient>(new MongoClient("mongodb://docker:mongopw@localhost:55000"));

builder.Services.AddMyVodStripe(_ =>
{
    _.ApiKey = "sk_test_51L3x0jIP4aoyQ2vi0RgDKWCPmI22ugYLYJbCeUBrgfYMEE0HBegyc5sJDt06xeEc1PQVtXqv56by02gLDwXZyU8Q00QT28pWGf";
    _.SuccessUrl = "https://example.com";
    _.ErrorUrl = "https://example.com";
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "MyArea",
    pattern: "{area:exists}/{controller=Dashboard}/{action=Index}/{id?}");
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();
