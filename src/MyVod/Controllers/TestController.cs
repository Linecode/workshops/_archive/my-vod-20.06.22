using MediatR;
using Microsoft.AspNetCore.Mvc;
using MyVod.Domain.Legal.Application;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.MovieCatalog.Application.Queries;
using MyVod.Domain.MovieCatalog.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Controllers;

[ApiController]
[Route("/[controller]")]
public class TestController : Controller
{
    private readonly ISender _sender;
    private readonly ILicesnseApplicationService _licenseApplicationService;

    public TestController(ISender sender, ILicesnseApplicationService licenseApplicationService)
    {
        _sender = sender;
        _licenseApplicationService = licenseApplicationService;
    }

    [HttpGet("/test/create-licesnse")]
    public async Task<IActionResult> CreateLicense()
    {
        var yestarday = DateTime.UtcNow.Subtract(TimeSpan.FromDays(1));
        var tomorrow = DateTime.UtcNow.Add(TimeSpan.FromDays(1));
        var dateRange = DateRange.Create(yestarday, tomorrow);
        var region = RegionIdentifier.Poland;
        var movieId = Guid.Parse("D931C99A-141C-4DDA-AC85-F62DB3EFB339");

        await _licenseApplicationService.AddLicense(dateRange, movieId, region);

        return Ok();
    }

    [HttpGet("/licesnse/{id:guid}")]
    public async Task<IActionResult> GetLicesnse(Guid id)
    {
        var result = await _licenseApplicationService.GetLicense(id);

        return Ok(result);
    }

    [HttpGet("/test/{movieId:guid}")]
    // ReSharper disable once RouteTemplates.ParameterTypeAndConstraintsMismatch
    public async Task<IActionResult> GetMovie(MovieId movieId)
    {
        var result = await _sender.Send(new GetMovieQuery(movieId));

        return Json(result);
    }

    [HttpGet("/test/{t}")]
    public async Task<IActionResult> GetMovie(string t)
    {
        var result = await _sender.Send(new GetAvilableMoviesQuery(t));

        return Json(result);
    }
}