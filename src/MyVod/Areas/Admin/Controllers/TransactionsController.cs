using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyVod.Areas.Admin.ViewModels;
using MyVod.Infrastructure.Models;
using MyVod.Services;

namespace MyVod.Areas.Admin.Controllers;

[Authorize]
[Area("admin")]
public class TransactionsController : Controller
{
    private readonly IOrderService _orderService;

    public TransactionsController(IOrderService orderService)
    {
        _orderService = orderService;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var orders = await _orderService.Get();

        var model = new TransactionsPageModel
        {
            OnGoing = orders.Where(x => x.Status == OrderStatus.Pending).ToList(),
            Completed = orders.Where(x => x.Status != OrderStatus.Pending).ToList()
        };
        
        return View("Index", model);
    }
}